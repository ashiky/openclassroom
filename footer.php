<a class="nav-link dropdown-toggle border-bottom border-top pt-5 pb-4" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown"
    aria-expanded="false">
    OPENCLASSROOMS
</a>
<ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
    <li><a class="dropdown-item" href="#">Qui sommes-nous ?</a></li>
    <li><a class="dropdown-item" href="#">Alternance</a></li>
    <li><a class="dropdown-item" href="#">Financements</a></li>
    <li><a class="dropdown-item" href="#">Experiences de formation</a></li>
    <li><a class="dropdown-item" href="#">Forum</a></li>
    <li><a class="dropdown-item" href="#">Blog</a></li>
    <li><a class="dropdown-item" href="#">Presse</a></li>
</ul>
<a class="nav-link dropdown-toggle border-bottom pt-4 pb-4" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown"
    aria-expanded="false">
    OPPORTUNITES
</a>
<ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
    <li><a class="dropdown-item" href="#">Nous rejoindre</a></li>
    <li><a class="dropdown-item" href="#">Devenir mentor</a></li>
    <li><a class="dropdown-item" href="#">Devenir coach carrière</a></li>
    <li>
        <hr class="dropdown-divider">
    </li>
    <li><a class="dropdown-item" href="#">Something else here</a></li>
</ul>
<a class="nav-link dropdown-toggle border-bottom pt-4 pb-4" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown"
    aria-expanded="false">
    AIDE
</a>
<ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
    <li><a class="dropdown-item" href="#">Action</a></li>
    <li><a class="dropdown-item" href="#">Another action</a></li>
    <li>
        <hr class="dropdown-divider">
    </li>
    <li><a class="dropdown-item" href="#">Something else here</a></li>
</ul>
<a class="nav-link dropdown-toggle border-bottom pt-4 pb-4" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown"
    aria-expanded="false">
    POUR LES ENTREPRISES
</a>
<ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
    <li><a class="dropdown-item" href="#">Formation,reconversion et alternance</a></li>
</ul>
<a class="nav-link dropdown-toggle border-bottom pt-4 pb-4" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown"
    aria-expanded="false">
    EN PLUS
</a>
<ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
    <li><a class="dropdown-item" href="#">Boutique</a></li>
    <li><a class="dropdown-item" href="#">Mentions légales</a></li>
    <li><a class="dropdown-item" href="#">Conditions générales d'utilisations</a></li>
    <li><a class="dropdown-item" href="#">Politiques de protection des données personneles</a></li>
    <li><a class="dropdown-item" href="#">Cookies</a></li>
    <li><a class="dropdown-item" href="#">Accessibilité</a></li>
    <li>
        <hr class="dropdown-divider">
    </li>
    <li><a class="dropdown-item" href="#">Something else here</a></li>
</ul>