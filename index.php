<?php
include("header.php");
?>
<div class="violet pt-5 ps-2">
  <div class="d-flex flex-column text-white">
    <h1>Votre avenir commence ici</h1>
    <p>Apprenez à apprendre, découvrez les compétences de demain, et prenez votre carrière en main.</p>
    <div class="col-6">
      <button type="button" class="btn btn-primary btn-lg m-2 bg-light textViolet">Démarrer ma candidature</button>
      <button type="button" class="btn btn-outline-primary btn-lg m-2 text-white border-white">Découvrir les formations</button>
      <img class="img-fluid" src="img/imageAccueil.jpeg" alt="">
    </div>
  </div>
  <div class="col-6">
    <span></span>
  </div>
</div>

<main class="p-2">
  <div class="container">
    <div class="card border-0">
      <img src="/img/diplome1.svg" class="m-auto" alt="...">
      <div class="card-body">
        <p class="fw-bold text-center">Diplômes accrédités</p>
        <p class="card-text">Le niveau de nos parcours certifiants est reconnu par l'État français</p>
      </div>
      <div class="card border-0">
        <img src="/img/diplome2.svg" class="m-auto" alt="...">
        <div class="card-body">
          <p class="fw-bold text-center">Mentorat</p>
          <p class="card-text">Chaque semaine, un expert du métier visé suit vos progrès</p>
        </div>
      </div>
      <div class="card border-0">
        <img src="/img/diplome3.svg" class="m-auto" alt="...">
        <div class="card-body ">
          <p class="fw-bold text-center">Embauché ou remboursé</p>
          <p class="card-text">Vous trouvez un emploi sous 6 mois ou nous remboursons vos frais de scolarité*</p>
        </div>
      </div>
    </div>
  </div>
  <h3 class="text-white violet w-75 p-2">Des formations innovantes</h3>
  <img class="img-fluid" src="/img/video.png" alt="">
  <p>
    Devenez qui vous voulez être avec OpenClassrooms. Choisissez votre carrière.
    <a class="text-black" href="#">Suivez une formation constituée de projets professionnalisants</a> et de séances
    individuelles avec un mentor dédié chaque semaine.
    Obtenez un diplôme reconnu par l'État. Enrichissez votre CV avec les <a class="text-black" href="#">programmes en
      alternance</a> proposés par OpenClassrooms
    et gagnez un salaire tout en suivant votre formation.
  </p>
  <h3 class="fw-bold text-center">Nos étudiants témoignent</h3>

  <!-- <?php include("carousel.php") ?> -->

  <img class="img-fluid" src="/img/pierre-eric.avif" alt="">
  <p>
    «&nbsp;En une phrase, une formation OpenClassrooms, c'est du challenge et du contenu de qualité.
    Dans la Tech, ce n'est pas tant ce qu'on sait qui compte,
    c'est ce qu'on est capable d'apprendre. Chez OpenClassrooms,
    on apprend vraiment à pêcher plutôt que d'attendre le poisson tout cuit dans le bec chaque jour.
    C'est donc un retour sur investissement vraiment intéressant&nbsp;!&nbsp;»
  </p>
  <p class="fw-bold">Pierre-Éric</p>
  <p class="fw-bold">Développeur web junior</p>
  <div class="container w-100 d-flex">
    <img src="/img/tel.svg" class="m-auto" alt="">
  </div>

  <h3 class="fw-bold">Nous sommes là pour vous aider</h3>
  <p>Vous avez une question ? Notre équipe est là pour vous répondre du lundi au vendredi de 9h00 à 19h00, heure en
    France métropolitaine.</p>

  <div class="container d-flex flex-row w-75 m-0 ps-2">
    <i class="fa-solid fa-envelope p-1 textViolet"></i>
    <p class="ps-2 textViolet">Nous écrire</p>
    <h3 class="ps-2 textViolet">|</h3>
    <i class="fa-solid fa-phone p-1 textViolet"></i>
    <p class="ps-2 textViolet">+33 1 80 88 80 30</p>
  </div>
  <p>* Sous réserve des conditions énoncées dans les <a class="text-black" href="#">Conditions Générales de Services</a>
  </p>

  <?php include("footer.php") ?>
  
  <script src="/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"></script>
</main>
</body>

</html>